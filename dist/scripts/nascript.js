    var callSatellite = function(status) {
          var nativeBooleen = status;
          console.log("satellite function called status set to  " + nativeBooleen);
          _satellite.setVar('NA Status ', nativeBooleen);
          _satellite.notify('NA Status: ' + nativeBooleen);
          _satellite.track('NA Status=' + nativeBooleen);
          }
          $("#nonNativeAmericanCheck").on('click', callSatellite(false));
          
          $("#nativeAmericanCheck").on("click", callSatellite(true));