
        // =========================================================================== //
        Jeremy Treadwell
        //class for the articles page. using handlebars, and json to pull in data
        if ($("body").hasClass("articles")) {
            var articles_method = {
                load_article_data: function() {
                    $.getJSON("scripts/articles_data.json", function(data) {
                        var templateSource = $("#article_list").html();
                        var template = Handlebars.compile(templateSource);
                        var article_html = template(data);
                        $('.article_list_container').html(article_html);
                        //sorry, this is a little hack for the MixItUp plugin
                        setTimeout(function() {
                            $(".article_list_container").mixItUp({
                                layout: {
                                    display: 'block'
                                }
                            });
                        }, 100);
                    });
                }
            };
            articles_method.load_article_data();
        }


 script(type="text/template" id="article_list")

     div
       {{#each articles}}
       div(class="article_copy clearfix mix {{this.filterClass}}", data-time="{{this.time}}", data-cost="{{this.cost}}", data-alpha-order="{{this.header}}")
         img(src="{{this.imageSrc}}", class="img_thumbnail")
         h2 {{this.header}}
         p {{this.text}}
         span.blue_sign.clock.fa.fa-clock-o &nbsp;
           span {{this.time_length}}
         span.fa.fa-usd.dollar_sign.blue_sign
         span.fa.fa-usd.dollar_sign.ltgray
         span.fa.fa-usd.dollar_sign.ltgray
         span.fa.fa-usd.dollar_sign.ltgray
         span.blue_sign.category 
           span {{this.category_tag_name}}
         .blackRule
       {{/each}}
